    .global histogram
    .text

histogram:
    xor     %rax, %rax
    xor     %rbx, %rbx
    call    rec
    ret

rec:
    mov     (%rdi, %rax, 1), %bl
    cmpb    $0, %bl
    je      end
    inc     %rax
    and     $31, %rbx
    dec     %rbx
    lea     (%rsi, %rbx, 4), %r9
    addl    $1, (%r9)
    jmp     rec

end:
    lea     (%rdi), %rax
    xor     %rbx, %rbx
    xor     %r9, %r9
    ret

