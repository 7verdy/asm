#include <err.h>
#include <stdlib.h>
#include <stdio.h>

void print_char_at(char *s, int index);

int main(int argc, char **argv)
{
    if (argc != 3)
        errx(1, "Usage: %s <string> <index>", argv[0]);

    print_char_at(argv[1], atoi(argv[2]));

    return 0;
}
